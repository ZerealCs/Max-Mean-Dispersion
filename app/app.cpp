#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include "../src/Problem.hpp"
#include "../src/Solution.hpp"
#include "../src/algorithms/BuilderGreedy.hpp"
#include "../src/algorithms/DestructorGreedy.hpp"
#include "../src/algorithms/Grasp.hpp"
#include "../src/algorithms/MultiRun.hpp"
#include "../src/algorithms/VNS.hpp"

using namespace std;

void exec(Problem problem) {
  int param1;
  double alpha;
  cout << "Selecione el algoritmo con el que ejecutar" << endl
       << "1 → BuilderGreedy" << endl
       << "2 → DestructorGreedy" << endl
       << "3 → GRASP" << endl
       << "4 → MultiRun" << endl
       << "5 → Skewed VNS" << endl;
  int option;
  cin >> option;
  Solution solution = Solution::empty(&problem);
  switch (option) {
    case 1:
		solution = BuilderGreedy().solve(&problem);
		break;
	case 2:
		solution = DestructorGreedy().solve(&problem);
		break;
	case 3:
    cout << "Introduzca el tamaño de la LRC" << endl;
    cin >> param1;
		solution = Grasp(param1).solve(&problem);
		break;
	case 4:
    cout << "Introduzca el número de iteraciones" << endl;
    cin >> param1;
		solution = MultiRun(param1).solve(&problem);
		break;
	case 5:
    cout << "Introduzca el número de iteraciones" << endl;
    cin >> param1;
    cout << "Introduzca el valor de alpha" << endl;
    cin >> alpha;
		solution = VNS(10, 1.5, diffSolution).solve(&problem);
		break;
  }
  solution.print(cout);
  cout << solution.getCost() << endl;
}


int main(int argc, char const* argv[])
{
	string name = argv[0];
	if (argc == 2) {
		srand(time(NULL));
		fstream file(argv[1]);
		Problem problem = Problem(file);
		exec(problem);
	}
	else {
		cout << "USAGE: " << name << " filename" << endl;
	}

	return 0;
}

