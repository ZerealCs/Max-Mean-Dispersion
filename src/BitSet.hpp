/*
 * =====================================================================================
 *
 *       Filename:  BitSet.hpp
 *
 *    Description:  Implementación de un bitset con metodos basicos
 *
 *        Version:  1.0
 *        Created:  25/04/16 15:13:03
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */


#pragma once

#include <functional>
#include <vector>
#include <cmath>

const int INT_SIZE = sizeof(unsigned int)*8;

/**
 * Representa un vector de booleanos eficiente en espacio, y comparaciones
 */
class BitSet {
private:
	// Size of bitset
	int size;

	vector<unsigned int> bitset;

  /**
   * Obtengo la posicion segun el tamaño del int en bits
   */
	int getPos(int ix) {
		return floor(float(ix) / float(INT_SIZE));
	}

public:
	BitSet(int size):
		size(size),
		bitset(ceil(float(size)/float(INT_SIZE)), 0)
	{
	}

  /**
   * Compara dos bitsets, son iguales?
   */
	bool isEqual(BitSet bitS) {
		return bitS.bitset == bitset;
	}

  /**
   * Rellena el bit set a 1s, el conjunto de todos los elementos dentro del tamaño size
   */
	void fillToOne(void) {
		unsigned int one = ~0;
		for (int i = 0; i < bitset.size(); i++) {
			bitset[i] = one;
		}
	}

  /**
   * Permite establecer un bit a 1 o 0, usando los respectivos booleanos
   */
	void setBit(int ix, bool value) {
		if (size >= ix) {
			if (value) {
				bitset[getPos(ix)] = bitset[getPos(ix)] ^ (1 << (ix % INT_SIZE));
			}
			else {
				bitset[getPos(ix)] = ~(~bitset[getPos(ix)] ^ (1 << (ix % INT_SIZE)));
			}
		} else {
			throw -1;
		}
	}

  /**
   * Obtiene el valor en bool del bit en la posicion `ix`
   */
	bool getBit(int ix){
		if (size >= ix) {
			return (bitset[getPos(ix)] & (1 << (ix % INT_SIZE))) != 0;
		}
		else {
			throw -1;
		}
	}

  /**
   * Permite recorrer el bitset aplicando una funcion sobre cada valor
   * la función puede conocer el indice y valor por el que va
   */
	void mapBitSet(function<void(int, bool)> func) {
		for (int i = 0; i < getSize(); i++) {
      func(i, getBit(i));
    }
  }

  /**
   * Retorna el tamaño del bitSet
   */
	int getSize(void) {
		return size;
	}

  /**
   * Imprime sobre un ostream el bitset
   */
	void print(ostream & os) {
		mapBitSet([&] (int, bool value) {
        os << value << ", ";
      });
		os << endl;
	}
};
