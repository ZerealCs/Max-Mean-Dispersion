
#include "Solution.hpp"

Solution::Solution(Problem * problem) :
	problem(problem),
	solution((int)problem->size()),
	cost(0),
	numSelectNodes(0)
{
}


Solution Solution::empty(Problem * problem) {
	return Solution(problem);
}


Solution Solution::random(Problem * problem) {
	auto aux = Solution::empty(problem);

	aux.mapOverAllNodes([&](int ix, bool select) {
			if ((rand() % 100) > 50) {
				aux.addNode(ix);
			}
	});
	if (aux.numSelectNodes == 0) {
		aux.addNode(1);
	}
	return aux;
}


Solution Solution::pairNode(int nodeI, int nodeJ, Problem * problem) {
	Solution sol = Solution(problem);
	sol.addNode(nodeI);
	sol.addNode(nodeJ);
	return sol;
}


Solution Solution::full(Problem * problem) {
	double costAcc = 0;
	problem->mapOverEdges([&](int nodeA, int nodeB, double cost) {
		costAcc += cost;
	});
	Solution solution = Solution(problem);
	solution.cost = costAcc;
	solution.solution.fillToOne();
	solution.numSelectNodes = problem->size();
	return solution;
}


void Solution::addNode(int nodeB) {
	// Calculate the new cost, with last cost
	mapOverSelectNodes([=](int nodeA) {
		cost += getProblem()->getWeight(nodeA, nodeB);
	});
	solution.setBit(nodeB-1, true);
	numSelectNodes += 1;
}


void Solution::removeNode(int nodeB) {
	// Calculate the new cost, with last cost
	numSelectNodes -= 1;
	solution.setBit(nodeB - 1, false);
	mapOverSelectNodes([=](int nodeA) {
		cost -= getProblem()->getWeight(nodeA, nodeB);
	});
}

void Solution::flipNode(int node) {
	if (solution.getBit(node - 1)) {
		removeNode(node);
	} else {
		addNode(node);
	}
}

bool Solution::getNode(int ix) {
	return solution.getBit(ix);
}

int Solution::size(void) {
	return problem->size();
}

void Solution::mapOverFreeNodes(function<void(int)> func) {
	mapOverAllNodes([=](int ix, bool select) {
		if (!select) {
			func(ix);
		}
	});
};


void Solution::mapOverSelectNodes(function<void(int)> func) {
	mapOverAllNodes([=](int ix, bool select) {
		if (select) {
			func(ix);
		}
	});
}


void Solution::mapOverAllNodes(function<void(int, bool)> func) {
	// TODO: Debo mejorar este bucle
	for (size_t i = 0; i < solution.getSize(); i++) {
		func(i+1, solution.getBit(i));
	}
}


void Solution::print(ostream & os) {
	mapOverSelectNodes([&] (int ix) {
		os << ix <<", ";
	});
	os << endl;
}
