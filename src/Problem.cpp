#include "Problem.hpp"


Weight& Problem::get(int nodeI, int nodeJ) {
	if (nodeI == nodeJ) {
		throw - 1; // no se almacena en la diagonal
	}
	if (nodeI > nodeJ) { // solo se almacena en la triangular superior
		return graph[nodeJ - 1][nodeI - nodeJ - 1];
	}
	else {
		return graph[nodeI - 1][nodeJ - nodeI - 1];
	}
}

Weight Problem::get(int nodeI, int nodeJ) const {
	if (nodeI == nodeJ) {
		throw - 1; // no se almacena en la diagonal
	}
	if (nodeI > nodeJ) { // solo se almacena en la triangular superior
		return graph[nodeJ - 1][nodeI - nodeJ - 1];
	}
	else {
		return graph[nodeI - 1][nodeJ - nodeI - 1];
	}
}

Problem::Problem(void) : graph() {};


Problem::Problem(int size) : graph(size - 1) {
	for (unsigned i = 0; i < graph.size(); i++) {
		graph[i] = vector<Weight>(graph.size() - i); // quitamos 11,22,33
		for (unsigned j = 0; j < graph[i].size(); j++) {
			if (rand() % 2 == 0) {
				graph[i][j] = (1 + rand() % 100);
			}
			else {
				graph[i][j] = (-1) * (1 + rand() % 100);
			}
		}
	}
}


Problem::Problem(istream & is) : graph() {
	int size;
	is >> size;

	graph = vector<vector<Weight> >(size - 1); // La fila que quitamos es la de un elemento de la triangular superior
	for (unsigned i = 0; i < graph.size(); i++) {
		graph[i] = vector<Weight>(graph.size() - i, -1); // se inicializa a -1 todos los valores, quitamos 11,22,33
	}

	for (unsigned i = 0; i < graph.size(); i++) {
		graph[i] = vector<Weight>(graph.size() - i); // quitamos 11,22,33
		for (unsigned j = 0; j < graph[i].size(); j++) {
			Weight weight;
			is >> weight;
			graph[i][j] = weight;
		}
	}
}


Problem::Problem(int size, vector<tuple<int, int, Weight> > graphI) : graph(size - 1) {
	for (unsigned i = 0; i < graph.size(); i++) {
		graph[i] = vector<Weight>(graph.size() - i, -1); // se inicializa a -1 todos los valores, quitamos 11,22,33
	}

	for (size_t i = 0; i < graphI.size(); i++) {
		get(std::get<0>(graphI[i]), std::get<1>(graphI[i])) = std::get<2>(graphI[i]);
	}
}


vector<int> Problem::getConnections(int node) {
	vector<int> connections;
	// NOTA: la matriz empieza en 11
	for (unsigned i = 1; i < graph.size() + 1; i++) {
		if (getWeight(node, i + 1) > 0) {
			connections.push_back(i + 1);
		}
	}
	return connections;
}

void Problem::mapOverEdges(function<void (int, int, Weight)> func) {
	for (size_t i = 1; i <= graph.size() + 1; i++) {
		for (size_t j = i+1; j <= graph.size() + 1; j++) {
			func(i, j, getWeight(i, j));
		}
	}
}


inline Weight Problem::getWeight(int nodeI, int nodeJ) const {
	if (nodeI == nodeJ) {
		return 0;
	}
	double aux = get(nodeI, nodeJ);
	return aux;
}

unsigned int Problem::size(void) {
	return graph.size() + 1;
}