/*
 * =====================================================================================
 *
 *       Filename:  Solution.hpp
 *
 *    Description:  Represent a solution of Problem class, it is generate by
 *    				algorithms
 *
 *        Version:  1.0
 *        Created:  16/04/16 19:19:07
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado (eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once

#include <vector>
#include <functional>
#include <iostream>

#include "Problem.hpp"
#include "BitSet.hpp"


using namespace std;

/**
 * Representa las soluciones del problema, ademas se encarga de calcular el coste
 * de la solucion para ello tiene un puntero a una instancia de *Problem*
 * El Coste es precalculado cada vez que se hace una modificación sobre la
 * solución
 */
class Solution {
private:
	Problem * problem;     // Problem instance
	double cost;           // Accumulate cost
	int numSelectNodes;    // Num of nodes into solution
	BitSet solution;       // Representation of solution

protected:
  /*
   * Generate a basic solution, but not use out of class. Is preferable use
   * empty, random, pairNode...
   */
	Solution(Problem *);

public:

	/*
	 * Build a solution of size n in nodes empty
	 */
	static Solution empty(Problem * problem);

	/* Generate a solution random */
	static Solution random(Problem * problem);

	/* Generate a solution from a pair of nodes */
	static Solution pairNode(int nodeI, int nodeJ, Problem * problem);

	/* All nodes are include in solution */
	static Solution full(Problem * problem);

	/*
	 * Add a new node to solution. Not add a repeated node, this make a incorrect
	 * cost.
	 */
	void addNode(int node);

	/* Remove a node from solution it should exits, if not bad things happend */
	void removeNode(int node);

	/*
	 * Flip a node if is selected is change to not selected
	 */
	void flipNode(int node);

	/*
	 * Map over no seletioned nodes
	 */
	void mapOverFreeNodes(function<void(int)> func);

	/*
	 * Map over selectioned nodes
	 */
	void mapOverSelectNodes(function<void(int)> func);

	/*
	 * Map over all nodes, the function take index node and value
	 */
	void mapOverAllNodes(function<void(int, bool)> func);

	/*
	 * Get size of solution, total num of node possible to select
	 */
	int size(void);

	/*
	 * Get value of node if it is select o not
	 */
	bool getNode(int ix);

	/**
	 * Show a solution by ostream
	 */
	void print(ostream & os);


	/**
	 * Get cost
	 */
	double getCost(void) const {
		return cost / numSelectNodes;
	}

	/**
	 * Get Problem
	 */
	Problem * getProblem(void) {
		return problem;
	}

  /**
   * Is  equal two solutions
   */
	bool isEqual(Solution sol) {
		return solution.isEqual(sol.solution);
	}

	/* double calculate(void) const { */
	/* 	double accum = 0; */
	/* 	for (int i = 0; i <= solution.size(); i++) { */
	/* 		if (solution[i]) { */
	/* 			for (int j = i+1; j <= solution.size(); j++) { */
	/* 				if (solution[j]) { */
	/* 					accum += problem->getWeight(i + 1, j+ 1); */
	/* 				} */
	/* 			} */
	/* 		} */
	/* 	} */
	/* 	return accum; */
	/* } */

};
