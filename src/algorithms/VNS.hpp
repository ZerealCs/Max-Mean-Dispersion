/*
 * =====================================================================================
 *
 *       Filename:  VNS.hpp
 *
 *    Description:  Make a search in its several enviroments
 *
 *        Version:  1.0
 *        Created:  24/04/16 09:27:50
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once


#include "../Problem.hpp"
#include "../Solution.hpp"
#include "Algorithm.hpp"
#include "BuilderGreedy.hpp"
#include "../eval/LocalSearch.hpp"
#include "../eval/Interchange.hpp"
#include "../eval/Shake.hpp"

using namespace std;

double diffSolution(Solution solutionA, Solution solutionB);


class VNS : public Algorithm {
private:
	int iterations;
	double alpha;
	function<double (Solution, Solution)> distance;
public:

	VNS(int iterations, double alpha, function<double (Solution, Solution)> func);

	Solution solve(Problem * problem);
};

