/*
 * =====================================================================================
 *
 *       Filename:  MultiRun.hpp
 *
 *    Description:  Multiple run to search best optimal local possible
 *
 *        Version:  1.0
 *        Created:  23/04/16 22:03:37
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once

#include <cmath>

#include "../Problem.hpp"
#include "../Solution.hpp"
#include "Algorithm.hpp"
#include "Grasp.hpp"
#include "../eval/LocalSearch.hpp"
#include "../eval/Interchange.hpp"


using namespace std;

class MultiRun : public Algorithm {
private:
	int iterations;
public:

	MultiRun(int iter);

	Solution solve(Problem * problem);
};

