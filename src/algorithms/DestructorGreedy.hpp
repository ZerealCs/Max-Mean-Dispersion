/*
* =====================================================================================
*
*       Filename:  DestructorGreedy.hpp
*
*    Description:  A destructor greedy algorithm declared in task.
*
*        Version:  1.0
*        Created:  16/04/16 18:18:24
*       Revision:  none
*       Compiler:  clang
*
*         Author:  Eleazar D�az Delgado(eleazardzdo at gmail dot com),
*     University:  ULL
*
* =====================================================================================
*/

#pragma once
#include "Algorithm.hpp"
#include "../eval/MaximumRemoving.hpp"


class DestructorGreedy : public Algorithm {
public:
	DestructorGreedy();
	Solution solve(Problem * problem);
	~DestructorGreedy();
};

