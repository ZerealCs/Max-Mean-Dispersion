
#include "MultiRun.hpp"

MultiRun::MultiRun(int iter):
	Algorithm("MultiRun iter: " + to_string(iter)),
	iterations(iter) {
}

/* Solution MultiRun::solve(Problem * problem) { */
/* 	auto interchange = Interchange(); */
/* 	//auto currentSol = Grasp(ceil(problem->size()*0.3)).solve(problem); */
/* 	auto currentSol = Solution::random(problem); */
/* 	auto best = currentSol; */

/* 	for (int i = 0; i < iterations; i++) { */
/* 		Solution localSearch = LocalSearch(&interchange).eval(currentSol); */
/* 		if (localSearch.getCost() >= best.getCost()) { */
/* 			best = localSearch; */
/* 		} */
/* 		auto currentSol = Solution::random(problem); */
/* 	} */

/* 	return best; */
/* } */



Solution MultiRun::solve(Problem * problem) {
	auto interchange = Interchange();
	auto currentSol = Grasp(ceil(problem->size()*0.3)).solve(problem);
	//auto currentSol = Solution::random(problem);
	auto best = currentSol;

	for (int i = 0; i < iterations; i++) {
		int change = problem->size()*0.5;
		currentSol.mapOverAllNodes([&] (int ix, bool select) {
			if (change > 0) {
				change--;
				if ((rand() % 100) >= 50) {
					if (select) {
						currentSol.removeNode(ix);
					}
					else {
						currentSol.addNode(ix);
					}
				}
			}
		});
		Solution localSearch = LocalSearch(&interchange).eval(currentSol);
		if (localSearch.getCost() >= best.getCost()) {
			best = localSearch;
		}
		else {
			currentSol = best;
		}
	}

	return best;
}
