#include "VNS.hpp"


double diffSolution(Solution solutionA, Solution solutionB) {
	double accumDiff = 0;
	solutionA.mapOverAllNodes([&] (int ix, bool select) {
		if (solutionB.getNode(ix) != select) {
			accumDiff += 1;
		}
	});

	return accumDiff;
}


VNS::VNS(int iterations, double alpha, function<double (Solution, Solution)> func):
	Algorithm("VNS iter: " + to_string(iterations)
			+ " alpha: " + to_string(alpha)),
	iterations(iterations),
	alpha(alpha),
	distance(func){
}

Solution VNS::solve(Problem * problem) {
	auto interchange = Interchange();
	auto shaker = Shake();
	// auto best = BuilderGreedy().solve(problem);
	auto best = Solution::random(problem); // Es mejor para ciertos casos
	auto current = best;

	for (int i = 0; i < iterations; i++) {
		int k = 0;
		do {
			Solution shake = shaker.in(k).eval(current);
			Solution localSearch = LocalSearch(&interchange).eval(shake);
			if (localSearch.getCost() >= best.getCost()) {
				best = localSearch;
			}
			if (localSearch.getCost() >= (current.getCost() + alpha * distance(localSearch, current))) {
				current = localSearch;
				k = 1;
			}
			else {
				k += 1;
			}
		} while(k == shaker.kMax());
	}

	return best;
}

