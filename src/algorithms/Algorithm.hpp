/*
 * =====================================================================================
 *
 *       Filename:  Algorithm.hpp
 *
 *    Description:  Abstract class to define a algorithm like a function from
 *    				Problem to Solution
 *
 *        Version:  1.0
 *        Created:  16/04/16 18:18:24
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once
//#ifndef ALGORITHM
//#define ALGORITHM

#include "../Problem.hpp"
#include "../Solution.hpp"

class Algorithm {
private:
	string name = "MeanMaxDispersion";

public:
	Algorithm(string name) : name(name) {};

	virtual Solution solve(Problem * problem) = 0;

	string getName(void) {
		return name;
	};
};

//#endif /* end of include guard: ALGORITHM */
