/*
 * =====================================================================================
 *
 *       Filename:  OneOfBest.hpp
 *
 *    Description:  Define choose random between RCL
 *
 *        Version:  1.0
 *        Created:  23/04/16 19:34:18
 *       Revision:  none
 *       Compiler:  clang
 * 
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once

#include <cstdlib>
#include <vector>

#include "../Eval.hpp"


class OneOfBest : public Eval {
	/*
	 * size of RCL
	 */
	int size;
public:
	OneOfBest(int size);

	Solution eval(Solution solution);

};
