/*
* =====================================================================================
*
*       Filename:  MaximumRemoving.hpp
*
*    Description:  A destructor greedy algorithm declared in task.
*
*        Version:  1.0
*        Created:  16/04/16 18:18:24
*       Revision:  none
*       Compiler:  clang
*
*         Author:  Eleazar D�az Delgado(eleazardzdo at gmail dot com),
*     University:  ULL
*
* =====================================================================================
*/

#pragma once

#include "../Eval.hpp"

class MaximumRemoving : public Eval {
public:

	Solution eval(Solution solution);

};

