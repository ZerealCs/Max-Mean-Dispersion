#include "MaximumRemoving.hpp"


Solution MaximumRemoving::eval(Solution solution) {
	Solution best = solution;
	solution.mapOverSelectNodes([&](int node) {
		Solution aux = solution;
		aux.removeNode(node);
		if (best.getCost() <= aux.getCost()) {// < <= ???
			best = aux;
		}
	});
	return best;
}

