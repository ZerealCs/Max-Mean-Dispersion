/*
 * =====================================================================================
 *
 *       Filename:  Interchange.hpp
 *
 *    Description:  Defines an operation of change nodes check with no checked
 *
 *        Version:  1.0
 *        Created:  23/04/16 20:06:45
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once

#include "../Eval.hpp"

/**
 * Define operation of interchange choosing the best
 */
class Interchange : public Eval {
public:
	Solution eval(Solution solution);

};
