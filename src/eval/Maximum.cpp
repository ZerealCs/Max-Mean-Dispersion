#include "Maximum.hpp"


Solution Maximum::eval(Solution solution) {
	Solution best = solution;
	solution.mapOverFreeNodes([&](int node) {
		Solution aux = solution;
		aux.addNode(node);
		if (best.getCost() < aux.getCost()) {// < <= ???
			best = aux;
			// change sol. make a bool to improve
		}
	});
	return best;
};

