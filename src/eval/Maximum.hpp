/*
* =====================================================================================
*
*       Filename:  Maximum.hpp
*
*    Description:  A especific class to eval a solution getting maximize it
*
*        Version:  1.0
*        Created:  18/04/16 20:09:41
*       Revision:  none
*       Compiler:  clang
*
*         Author:  Eleazar D�az Delgado(eleazardzdo at gmail dot com),
*     University:  ULL
*
* =====================================================================================
*/
#pragma once


#include "../Eval.hpp"

class Maximum : public Eval {
public:

	Solution eval(Solution solution);
};

