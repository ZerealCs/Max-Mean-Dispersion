/*
 * =====================================================================================
 *
 *       Filename:  LocalSearch.hpp
 *
 *    Description:  Make a local search from a solution given
 *
 *        Version:  1.0
 *        Created:  24/04/16 08:30:09
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once

#include "../Eval.hpp"


/**
 * Defines a local search with a specific operation
 */
class LocalSearch : public Eval {
private:
	Eval * operation;

public:
	LocalSearch(Eval * operation): operation(operation) {
	};

	Solution eval(Solution solution) {
		Solution best = solution;
		bool stop = false;
		do {
			Solution newBest = operation->eval(best); // siempre se devuelve best o una solucion mejor
			if (newBest.isEqual(best)) {
				stop = true;
			}
			else {
				best = newBest;
			}
		} while(!stop);

		return best;
	}
};
